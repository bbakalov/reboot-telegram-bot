<?php

require_once 'vendor/autoload.php';

use \TelegramBot\Api\Client;

try {

    // include config
    $config = require_once 'config.php';

    // create a bot
    $bot          = new Client($config['bot_token']);
    $rawBody      = json_decode($bot->getRawBody());
    $inputCommand = substr($rawBody->message->text, 1);

    $allowCommands = [
        'all'      => getAll(),
        'liquids'  => getLiquids(),
        'contacts' => getContacts(),
        'delivery' => getDelivery(),
        'help'     => getHelp(),
        'start'    => getHelp(),
    ];

    $bot->command($inputCommand,
        function($message) use ($bot, $config, $allowCommands, $inputCommand) {
            if (array_key_exists($inputCommand, $allowCommands)) {
                $data = $allowCommands[$inputCommand];
            } else {
                $data = [
                    'text'                => '😔Нипанятна😔 /help',
                    'parseMode'           => null,
                    'disablePreview'      => false,
                    'replyToMessageId'    => null,
                    'replyMarkup'         => null,
                    'disableNotification' => false
                ];
            }
            /**
             * @var $bot \TelegramBot\Api\BotApi
             */
            $bot->sendMessage(
                $message->getChat()->getId(),
                $data['text'],
                $data['parseMode'],
                $data['disablePreview'],
                $data['replyToMessageId'],
                $data['replyMarkup'],
                $data['disableNotification']);
        });
    $bot->run();
} catch (\TelegramBot\Api\Exception $e) {
    // @todo add error log
    logger($e->getMessage());
}

function logger($text)
{
    file_put_contents(__DIR__ . '/log.log', "$text\n", FILE_APPEND);
}

function getAll()
{
    $text     = '
*Подробная информация про магазин и ассортимент* 
https://bit.ly/rebootvape';
    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
        [
            [
                ['text' => 'Перейти', 'url' => 'https://bit.ly/rebootvape']
            ]
        ]
    );

    return [
        'text'                => $text,
        'parseMode'           => 'Markdown',
        'disablePreview'      => false,
        'replyToMessageId'    => null,
        'replyMarkup'         => $keyboard,
        'disableNotification' => false
    ];
}

function getLiquids()
{
    $text     = '
[_](https://bit.ly/rebootvape)
*JUICER* 
8 разных вкусов
Цена: 60мл 150грн
Крепость: 0 мг | 1.5 мг | 3 мг
Подробная информация: [смотреть ](https://telegra.ph/JUICER--REBOOT-shop-09-23)
*Retrowave*
6 вкусов
Цена: 60мл 150грн
Крепость: 1.5 мг | 3 мг
Подробная информация: [смотреть](https://telegra.ph/Retrowave-e-juice-REBOOT-09-13)
*Fluffy Puff*
6 вкусов
Цена: 60мл 140грн
Крепость: 0 мг | 1.5 мг | 3 мг
Подробная информация: [смотреть](https://telegra.ph/Fluffy-Puff--REBOOT-shop-09-24)
*Mad Breakfast*
4 вкуса
Цена: 60мл 130грн
Крепость: 0 мг | 1.5 мг | 3 мг
Подробная информация: [смотреть](https://telegra.ph/Mad-Breakfast--REBOOT-shop-09-25)
';
    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
        [
            [
                ['text' => 'JUICER', 'url' => 'https://telegra.ph/JUICER--REBOOT-shop-09-23'],
                ['text' => 'Retrowave', 'url' => 'https://telegra.ph/Retrowave-e-juice-REBOOT-09-13'],
                ['text' => 'Fluffy Puff', 'url' => 'https://telegra.ph/Fluffy-Puff--REBOOT-shop-09-24'],
                ['text' => 'Mad Breakfast', 'url' => 'https://telegra.ph/Mad-Breakfast--REBOOT-shop-09-25']
            ]
        ]
    );
    return [
        'text'                => $text,
        'parseMode'           => 'Markdown',
        'disablePreview'      => false,
        'replyToMessageId'    => null,
        'replyMarkup'         => $keyboard,
        'disableNotification' => false
    ];
}

function getContacts()
{
    $text = '
Контакты для заказа
Телефон: +38(063)767-27-17
Telegram: https://t.me/rebootvape_bot
email: rebootvape@gmail.com
';
    return [
        'text'                => $text,
        'parseMode'           => null,
        'disablePreview'      => false,
        'replyToMessageId'    => null,
        'replyMarkup'         => null,
        'disableNotification' => false
    ];
}

function getDelivery()
{
    $text = '
[_](https://bit.ly/rebootvape)
Доставка по всей Украине через службу Новая Почта
Бесплатная доставка при заказе от 5 баночек';
    return [
        'text'                => $text,
        'parseMode'           => 'Markdown',
        'disablePreview'      => false,
        'replyToMessageId'    => null,
        'replyMarkup'         => null,
        'disableNotification' => false
    ];
}

function getHelp()
{
    $text = '
Используйте следующие кнопки:
/all
/liquids
/contacts
/delivery
';
    return [
        'text'                => $text,
        'parseMode'           => null,
        'disablePreview'      => false,
        'replyToMessageId'    => null,
        'replyMarkup'         => null,
        'disableNotification' => false
    ];
}
