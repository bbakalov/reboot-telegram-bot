## Botname
https://telegram.me/botname
## Bot commands
* **/all** - вся информация
* **/liquids** - все жидкости
* **/contacts** - контакты магазина
* **/delivery** - информация про доставку
* **/start** - отображение всех доступных команд
* **/help** - отображение всех доступных команд

## Lauch this bot on localhost
* Run local PHP server
```bash
 php -S localhost:9100
```
* install ngrok [https://ngrok.com/download] and
```bash
./ngrok http 9100

```
* create bot via https://t.me/BotFather
* write bot token to ./config.php
* take https:// link from ngrok and register webhook for current bot
```
https://api.telegram.org/botAPI_TOKEN/setwebhook?url=https://70ac5842.ngrok.io/index.php
```